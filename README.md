# Admin panel for bots (Telegram/FB/Viber)

Short desciption

## Getting Started

```
git clone https://gitlab.com/omentes/laravel-bots.git
cd laravel-bots/laradock
docker-compose up -d nginx mysql phpmyadmin redis workspace
```

Open website => [localhost](http://localhost/)
Open PhpMyAdmin => [PhpMyAdmin](http://localhost:8080/)


## Built With

* [Laravel](https://github.com/laravel/laravel) - Laravel is a web application framework with expressive, elegant syntax.
* [Laradock](http://laradock.io/) - A full PHP development environment for Docker.
* [laravel-admin](https://github.com/artem-tverdokhlebov/laravel-admin/tree/artem-dev) - laravel-admin is administrative interface builder for laravel which can help you build CRUD backends just with few lines of code.
* [Telegram Bot API - PHP SDK](https://github.com/artem-tverdokhlebov/telegram-bot-sdk) - The (Unofficial) Telegram Bot API PHP SDK. Lets you develop Telegram Bots easily! Supports Laravel out of the box.
* [FB Messenger Bot PHP API](https://github.com/pimax/fb-messenger-php) - This is a PHP implementation for Facebook Messenger Bot API.
* [PHP sdk for Viber api](http://www.example.org/) - Library to develop a bot for the Viber platform.

## Authors

* **Artem Pakhomov** - [omentes](https://gitlab.com/omentes)

## License

This project is licensed under the MIT License
