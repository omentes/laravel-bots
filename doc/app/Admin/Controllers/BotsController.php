<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Bots;

class BotsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */

    protected function new($add) {
        $add = new Bots;
        $add->platform = $data_input['platform'];
        $add->save;
    }


    public function index()
    {
        $allbots = Bots::all();

        return view('botsindex', ['$allbots' => $allbots]);
    }
}
