<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Bots;

class BotsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Bots')
            ->description('Description...')
            ->row(Bots::title())
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Bots::environment());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Bots::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Bots::dependencies());
                });
            });
    }

}
